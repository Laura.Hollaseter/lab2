package INF101.lab2;

import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    int maxCapacity = 20;
    ArrayList<FridgeItem> items =new ArrayList<>();

    

    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        return items.size();
    }

    @Override
    public int totalSize() {
        // TODO Auto-generated method stub
        return maxCapacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
        if(items.size()>= totalSize())
           return false;
        return items.add(item);

    }

    @Override
    public void takeOut(FridgeItem item) {

        if(!items.contains(item)) {
            throw new NoSuchElementException("Fridge does not contain item. ");
        }
        items.remove(item);
        
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = new ArrayList<>();
        for (FridgeItem item : items){
            if (item.hasExpired()){
                expiredItems.add(item);
            }

        }
        items.removeAll(expiredItems);
        return expiredItems;
                
    }


}
